FROM node:alpine
# FROM node:slim
MAINTAINER caner@candan.fr

# enable experimental workspaces yarn feature
RUN echo "workspaces-experimental true" > /usr/local/share/.yarnrc

# update repos
# RUN apt update -yqq

# install envsubst
RUN \
    apk add --update libintl && \
    apk add --virtual build_deps gettext &&  \
    cp /usr/bin/envsubst /usr/local/bin/envsubst && \
    apk del build_deps
# RUN apt install -yqq gettext-base

# install git required by lerna-publish
RUN apk add -U git python build-base
# RUN apt install -yqq git python build-essential

# install lerna
RUN npm i -g lerna

# install PR version of lerna
# RUN apk add -U openssl
# RUN cd /tmp && wget https://github.com/jezzay/lerna/archive/b4710e12b564c52672089bb1c0d0cbf85f0d170b.zip -O lerna.zip && unzip lerna.zip && mv lerna-* lerna && cd lerna && yarn

# start the command
CMD lerna publish --conventional-commits --yes
# CMD /tmp/lerna/bin/lerna.js publish --conventional-commits --yes
